# graphql-codegen-kotlin-dsl-plugin

A plugin for https://graphql-code-generator.com/ that creates a kotlin dsl for generating string representations of graphql responses based on a schema.

Based on https://graphql-code-generator.com/docs/custom-codegen/write-your-plugin
