import { processSchema } from './utils.test';

describe('graphql AST parser', () => {
  it('should handle custom types', () => {
    const visited = processSchema(`
      type Exmaple {
        name: String!
      }

      type Query {
        field: Exmaple!
      }
    `);

    expect(visited.definitions).toMatchInlineSnapshot(`
      Array [
        Object {
          "fields": Array [
            Object {
              "name": "name",
              "type": Object {
                "name": "String",
                "type": "scalar",
              },
            },
          ],
          "name": "Exmaple",
        },
        Object {
          "fields": Array [
            Object {
              "name": "field",
              "type": Object {
                "name": "Exmaple",
                "type": "object",
              },
            },
          ],
          "name": "Query",
        },
      ]
    `);
  });

  it('should handle deeply nested list types', () => {
    const visited = processSchema(`
      type Query {
        field: [[[String!]!]!]!
      }
    `);

    expect(visited.definitions).toMatchInlineSnapshot(`
      Array [
        Object {
          "fields": Array [
            Object {
              "name": "field",
              "type": Object {
                "name": "String",
                "type": "list",
              },
            },
          ],
          "name": "Query",
        },
      ]
    `);
  });

  it('should handle deeply nested nullable list types', () => {
    const visited = processSchema(`
      type Query {
        field: [[[String]]]
      }
    `);

    expect(visited.definitions).toMatchInlineSnapshot(`
      Array [
        Object {
          "fields": Array [
            Object {
              "name": "field",
              "type": Object {
                "name": "String",
                "type": "list",
              },
            },
          ],
          "name": "Query",
        },
      ]
    `);
  });

  it('should handle nullable types', () => {
    const visited = processSchema(`
      type Query {
        field: String
      }
    `);

    expect(visited.definitions).toMatchInlineSnapshot(`
      Array [
        Object {
          "fields": Array [
            Object {
              "name": "field",
              "type": Object {
                "name": "String",
                "type": "scalar",
              },
            },
          ],
          "name": "Query",
        },
      ]
    `);
  });

  it('should handle scalars and enums', () => {
    const visited = processSchema(`
      scalar Natural

      enum Locale {
        EN_US
        EN_UK
      }

      type Query {
        field: String
      }
    `);

    expect(visited.definitions).toMatchInlineSnapshot(`
      Array [
        Object {
          "fields": Array [
            Object {
              "name": "field",
              "type": Object {
                "name": "String",
                "type": "scalar",
              },
            },
          ],
          "name": "Query",
        },
      ]
    `);
  });
});
