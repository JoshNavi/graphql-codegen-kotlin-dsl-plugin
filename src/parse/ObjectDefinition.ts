/*
 * The `Input` and `Output` types describe the actual behavior of this function
 * whereas the `VisitedObjectTypeDefinitionNode` type describes its intended use
 *
 * Ideally there is some way to have the benefits of the generics
 * and actually describing the functions behavior
 * and we would also ensure that we only call the function with the desired types
 */
import { VisitedFieldDefinitionNode } from './FieldDefinition';

export type VisitedObjectTypeDefinitionNode = {
  name: string;
  fields: VisitedFieldDefinitionNode[];
};

type Input<T, U> = {
  name: {
    value: T;
  };
  fields: U;
};

type Output<T, U> = {
  name: T;
  fields: U;
};

const parse = <T, U>(node: Input<T, U>): Output<T, U> => ({
  name: node.name.value,
  fields: node.fields,
});

export default parse;
