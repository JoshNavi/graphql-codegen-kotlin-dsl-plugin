/*
 * Normally, `T` in this file should be `VisitedNamedTypeNode`
 * and `Input` would be similar to `NonNullTypeNode`
 * with swapping out the original `TypeNode` type for our `VisitedNamedTypeNode`
 * or our `VisitedListTypeNode`
 *
 * import { NonNullTypeNode } from 'graphql';
 * import { VisitedNamedTypeNode } from './NamedType';
 *
 * Ideally there is some way to have the benefits of the generics
 * and actually describing the functions behavior
 * and we would also ensure that we only call the function with the desired types
 */

type Input<T> = {
  type: T;
};

const parse = <T>(n: Input<T>): T => n.type;

export default parse;
