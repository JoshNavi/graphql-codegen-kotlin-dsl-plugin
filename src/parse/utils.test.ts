import { parse, visit } from 'graphql';
import parseNonNullType from './NonNullType';
import parseListType from './ListType';
import parseNamedType from './NamedType';
import parseFieldDefinition from './FieldDefinition';
import parseObjectDefinition from './ObjectDefinition';

export const processSchema = (schema: string) =>
  visit(parse(schema), {
    leave: {
      ScalarTypeDefinition: () => null,
      EnumTypeDefinition: () => null,
      NonNullType: parseNonNullType,
      ListType: parseListType,
      NamedType: parseNamedType({
        scalars: { String: 'String', Natural: 'Int' },
        enums: { Locale: 'String' },
      }),
      FieldDefinition: parseFieldDefinition,
      ObjectTypeDefinition: parseObjectDefinition,
    },
  });

describe('utils', () => {
  it('should pass the test', () => {
    expect(true).toBeTruthy();
  });
});
