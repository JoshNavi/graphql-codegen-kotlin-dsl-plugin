/*
 * Normally, `T` in this file should be `VisitedNamedTypeNode`
 * and `Input` would be similar to `ListTypeNode`
 * with swapping out the original `NamedTypeNode` type for our `VisitedNamedTypeNode`
 *
 * import { ListTypeNode } from 'graphql';
 * import { VisitedNamedTypeNode } from './NamedType';
 *
 * Ideally there is some way to have the benefits of the generics
 * and actually describing the functions behavior
 * and we would also ensure that we only call the function with the desired types
 */

type VisitedListTypeNode<T> = {
  type: 'list';
  name: T;
};

type Input<T> = {
  type: {
    name: T;
  };
};

const parse = <T>(n: Input<T>): VisitedListTypeNode<T> => ({
  name: n.type.name,
  type: 'list',
});

export default parse;
