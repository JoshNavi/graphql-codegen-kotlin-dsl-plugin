/*
 * The `Input` and `Output` types describe the actual behavior of this function
 * whereas the `VisitedFieldDefinitionNode` type describes its intended use
 *
 * Ideally there is some way to have the benefits of the generics
 * and actually describing the functions behavior
 * and we would also ensure that we only call the function with the desired types
 */
import { VisitedNamedTypeNode } from './NamedType';

export type VisitedFieldDefinitionNode = {
  name: string;
  type: VisitedNamedTypeNode;
};

type Input<T, U> = {
  name: {
    value: T;
  };
  type: U;
};

type Output<T, U> = {
  name: T;
  type: U;
};

const parse = <T, U>(node: Input<T, U>): Output<T, U> => ({
  name: node.name.value,
  type: node.type,
});

export default parse;
