import { NamedTypeNode } from 'graphql';
import { Config } from '../config';

type MappedTypes = Pick<Config, 'scalars' | 'enums'>;
type Kinds = 'list' | 'object' | 'scalar';

export type VisitedNamedTypeNode = {
  name: string;
  type: Kinds;
};

const toCustomType = ({ scalars, enums }: MappedTypes) => (t: string) =>
  scalars[t] ?? enums[t];

const toVisitTypeNode = (customType: string | undefined) => (t: string) =>
  ({
    name: customType ?? t,
    type: customType ? 'scalar' : 'object',
  } as const);

const parse = (types: MappedTypes) => (
  n: NamedTypeNode,
): VisitedNamedTypeNode =>
  toVisitTypeNode(toCustomType(types)(n.name.value))(n.name.value);

export default parse;
