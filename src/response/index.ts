import { parse, printSchema, visit } from 'graphql';
import {
  PluginFunction,
  PluginValidateFn,
} from '@graphql-codegen/plugin-helpers';
import {
  validateFile,
  validateConfig,
  parse as parseConfig,
  Config,
  RawConfig,
} from '../config';
import parseNonNullType from '../parse/NonNullType';
import parseListType from '../parse/ListType';
import parseNamedType from '../parse/NamedType';
import parseFieldDefinition from '../parse/FieldDefinition';
import parseObjectDefinition from '../parse/ObjectDefinition';
import type { VisitedObjectTypeDefinitionNode } from '../parse/ObjectDefinition';
import {
  toBuilderClass,
  toListBuilderClass,
  toScalarListBuilderClass,
} from '../print/builders';
import { toResponse } from '../print/response';
import { toPackage } from '../print/package';

const { log, error } = console;
const print = (x: unknown) => log(JSON.stringify(x, null, 2));

export const plugin: PluginFunction<Config> = (schema, _, rawConfig) => {
  const config = parseConfig(rawConfig);

  if (config.verbose) {
    log('Running graphql-codegen KotlinDSL plugin in verbose mode');
  }

  const ast = parse(printSchema(schema));

  const visitor = {
    ScalarTypeDefinition: () => null,
    EnumTypeDefinition: () => null,
    InputObjectTypeDefinition: () => null,
    NonNullType: parseNonNullType,
    ListType: parseListType,
    NamedType: parseNamedType({
      scalars: config.scalars,
      enums: config.enums,
    }),
    FieldDefinition: parseFieldDefinition,
    ObjectTypeDefinition: parseObjectDefinition,
  };

  const visited: { definitions: VisitedObjectTypeDefinitionNode[] } = visit(
    ast,
    {
      // TS doesn't like our custom node types which get returned from our parsers
      leave: visitor as any,
    },
  );

  const definitions = visited.definitions.filter((x) => x.name);

  if (config.verbose) {
    print(definitions);
  }

  const output = [toPackage(config.pkg)];

  [
    ...new Set([
      ...Object.values(config.scalars),
      ...Object.values(config.enums),
    ]),
  ].forEach((scalar) => {
    output.push(toScalarListBuilderClass(scalar));
  });

  definitions.forEach(({ name }) => {
    if (name !== 'Query') {
      output.push(toListBuilderClass(name));
    }
  });

  definitions.forEach(({ name, fields }) => {
    output.push(toBuilderClass(config)(name, fields));
  });

  output.push(toResponse());

  return output.join('\n\n');
};

export const validate: PluginValidateFn<RawConfig> = (_1, _2, config, file) => {
  validateConfig(config);

  if (!validateFile(file)) {
    error('Output file must be a kotlin file:');
    error(file);
    throw Error();
  }
};
