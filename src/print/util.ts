import { BLOCK_PARAM, BUILD_FUN, BUILDER_SUFFIX } from './consts';

export const toCamelCase = (s: string) =>
  s.charAt(0).toLowerCase() + s.slice(1);

export const toClosureType = (s: string) => `${BLOCK_PARAM}: ${s}.() -> Unit`;

export const toClosureApply = (s: string) =>
  `${s}().apply(${BLOCK_PARAM}).${BUILD_FUN}()`;

export const toBuilderClassName = (t: string) => `${t}${BUILDER_SUFFIX}`;

export const toListBuilderClassName = (t: string) =>
  `${t}List${BUILDER_SUFFIX}`;

export const isList = (n: any) => n.type === 'list';

export const isScalar = (n: any) => n.type === 'scalar';

export const isObject = (n: any) => n.type === 'object';
