import { VisitedFieldDefinitionNode } from '../parse/FieldDefinition';

export type PrintFn = (n: VisitedFieldDefinitionNode) => string;
