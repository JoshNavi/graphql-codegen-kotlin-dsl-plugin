import { toResponse } from './response';

describe('toResponse', () => {
  it('should print the response fun', () => {
    expect(toResponse()).toEqual(
      'fun response(block: QueryBuilder.() -> Unit): String = """{"data":""" + QueryBuilder().apply(block).build() + "}"',
    );
  });
});
