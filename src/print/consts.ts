// indents are 4 spaces
export const INDENT = '    ';
export const NEWLINE = '\n';
export const DOUBLE_NEWLINE = NEWLINE + NEWLINE;
export const BODY = 'body';
export const BUILDER_SUFFIX = 'Builder';
export const BLOCK_PARAM = 'block';
export const BUILD_FUN = 'build';
