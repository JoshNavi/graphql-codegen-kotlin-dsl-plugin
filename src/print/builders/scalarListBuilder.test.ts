import toScalarListBuilder from './scalarListBuilder';

const TYPE_LIST_BUILDER_CLASS = `class StringListBuilder {
    private val body = mutableListOf<String>()

    fun string(value: String) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}`;

const INT_TYPE_LIST_BUILDER_CLASS = `class IntListBuilder {
    private val body = mutableListOf<Int>()

    fun int(value: Int) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}`;

describe('scalar list types', () => {
  it('should return a scalar list type builder class', () => {
    expect(toScalarListBuilder('String')).toEqual(TYPE_LIST_BUILDER_CLASS);
  });

  it('should return a scalar list type builder class, for Int', () => {
    expect(toScalarListBuilder('Int')).toEqual(INT_TYPE_LIST_BUILDER_CLASS);
  });
});
