import { BODY, INDENT, BUILD_FUN } from '../consts';
import {
  toCamelCase,
  toClosureType,
  toClosureApply,
  toBuilderClassName,
  toListBuilderClassName,
} from '../util';

const toBody = (name: string) =>
  `${BODY} += ${toClosureApply(toBuilderClassName(name))}`;

const toFun = (name: string) => `${INDENT}fun ${toCamelCase(
  name,
)}(${toClosureType(toBuilderClassName(name))}) {
${INDENT + INDENT}${toBody(name)}
${INDENT}}`;

const toListBuilderClass = (name: string) => `class ${toListBuilderClassName(
  name,
)} {
${INDENT}private val ${BODY} = mutableListOf<String>()

${toFun(name)}

${INDENT}fun ${BUILD_FUN}() = ${BODY}.joinToString(
${INDENT + INDENT}separator = ",",
${INDENT + INDENT}prefix = "[",
${INDENT + INDENT}postfix = "]"
${INDENT})
}`;

export default toListBuilderClass;
