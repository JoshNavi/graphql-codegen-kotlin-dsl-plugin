import toBuilderClass from './builder';
import { toNode } from '../fields/doubles';

const TYPE_LIST_BUILDER_CLASS = `class TypeBuilder {
    private val body = mutableListOf<String>()



    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}`;

const TYPE_LIST_BUILDER_CLASS_WITH_FIELDS = `class TypeBuilder {
    private val body = mutableListOf<String>()

    fun field(field: Type) {
        body += """"field":"$field""""
    }

    fun a(a: String) {
        body += """"a":"$a""""
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}`;

const config: any = {
  scalars: { String: 'String', Int: 'Int', Type: 'Type' },
  wrapLiteralsInQuotes: {
    Type: true,
    String: true,
  },
};

describe('non scalar type class builder', () => {
  it('should return a type builder class with no fields', () => {
    expect(toBuilderClass(config)('Type', [])).toEqual(TYPE_LIST_BUILDER_CLASS);
  });

  it('should return a type builder class with some fields', () => {
    const fields = [
      toNode(),
      toNode({ name: 'a', typeName: 'String', typeType: 'scalar' }),
    ];
    expect(toBuilderClass(config)('Type', fields)).toEqual(
      TYPE_LIST_BUILDER_CLASS_WITH_FIELDS,
    );
  });
});
