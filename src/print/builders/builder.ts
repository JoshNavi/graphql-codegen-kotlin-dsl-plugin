import { Config } from '../../config';
import { BODY, INDENT, DOUBLE_NEWLINE, BUILD_FUN } from '../consts';
import { toBuilderClassName, isList, isScalar, isObject } from '../util';
import { toScalarFun, toListFun, toObjectFun } from '../fields';
import { VisitedFieldDefinitionNode } from '../../parse/FieldDefinition';

const toFun = (config: Config) => (field: VisitedFieldDefinitionNode) => {
  switch (true) {
    case isScalar(field.type):
      return toScalarFun(config)(field);
    case isObject(field.type):
      return toObjectFun(field);
    case isList(field.type):
      return toListFun(field);
    default:
      return '';
  }
};

const toBuilderClass = (config: Config) => (
  name: string,
  fields: VisitedFieldDefinitionNode[],
) => `class ${toBuilderClassName(name)} {
${INDENT}private val ${BODY} = mutableListOf<String>()

${fields.map(toFun(config)).join(DOUBLE_NEWLINE)}

${INDENT}fun ${BUILD_FUN}() = ${BODY}.joinToString(
${INDENT + INDENT}separator = ",",
${INDENT + INDENT}prefix = "{",
${INDENT + INDENT}postfix = "}"
${INDENT})
}`;

export default toBuilderClass;
