import toListBuilderClass from './listBuilder';

const TYPE_LIST_BUILDER_CLASS = `class TypeListBuilder {
    private val body = mutableListOf<String>()

    fun type(block: TypeBuilder.() -> Unit) {
        body += TypeBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}`;

const TYPE_NAME_LIST_BUILDER_CLASS = `class TypeNameListBuilder {
    private val body = mutableListOf<String>()

    fun typeName(block: TypeNameBuilder.() -> Unit) {
        body += TypeNameBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}`;

describe('list types', () => {
  it('should return a list type builder class', () => {
    expect(toListBuilderClass('Type')).toEqual(TYPE_LIST_BUILDER_CLASS);
  });

  it('should return a list type builder class with a 2 word type', () => {
    expect(toListBuilderClass('TypeName')).toEqual(
      TYPE_NAME_LIST_BUILDER_CLASS,
    );
  });
});
