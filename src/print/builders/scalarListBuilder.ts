import { BODY, INDENT, BUILD_FUN } from '../consts';
import { toCamelCase, toListBuilderClassName } from '../util';

const toBody = () => `${BODY} += value`;

const toFun = (name: string) => `${INDENT}fun ${toCamelCase(
  name,
)}(value: ${name}) {
${INDENT + INDENT}${toBody()}
${INDENT}}`;

const toScalarListBuilderClass = (
  name: string,
) => `class ${toListBuilderClassName(name)} {
${INDENT}private val ${BODY} = mutableListOf<${name}>()

${toFun(name)}

${INDENT}fun ${BUILD_FUN}() = ${BODY}.joinToString(
${INDENT + INDENT}separator = ",",
${INDENT + INDENT}prefix = "[",
${INDENT + INDENT}postfix = "]"
${INDENT})
}`;

export default toScalarListBuilderClass;
