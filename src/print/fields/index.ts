import { toFun as toListFun } from './list';
import { toFun as toObjectFun } from './object';
import { toFun as toScalarFun } from './scalar';

export { toListFun, toObjectFun, toScalarFun };
