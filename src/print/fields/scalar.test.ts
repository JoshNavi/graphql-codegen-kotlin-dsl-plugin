import { toFun } from './scalar';
import { toNode } from './doubles';

const FIELD_FUN_STRING = `    fun field(field: Type) {
        body += """"field":"$field""""
    }`;

const FIELD_FUN_INT = `    fun field(field: Int) {
        body += """"field":$field"""
    }`;

describe('scalars', () => {
  it('should return a field fun for a string scalar field', () => {
    const config: any = {
      scalars: { Type: 'Type' },
      wrapLiteralsInQuotes: {
        Type: true,
      },
    };

    expect(toFun(config)(toNode())).toEqual(FIELD_FUN_STRING);
  });

  it('should return a field fun for an int scalar field', () => {
    const config: any = {
      scalars: { Int: 'Int' },
      enums: { Int: 'Int' },
      wrapLiteralsInQuotes: {
        String: true,
      },
    };

    expect(
      toFun(config)(toNode({ typeType: 'scalar', typeName: 'Int' })),
    ).toEqual(FIELD_FUN_INT);
  });

  it('should return a field that resolves to int', () => {
    const config: any = {
      scalars: { Type: 'Int' },
      wrapLiteralsInQuotes: {
        String: true,
      },
    };

    expect(
      toFun(config)(toNode({ typeType: 'scalar', typeName: 'Type' })),
    ).toEqual(FIELD_FUN_INT);
  });
});
