import { toNode } from './doubles';
import { toFun } from './list';

const FIELD_FUN = `    fun field(block: TypeListBuilder.() -> Unit) {
        body += """"field":""" + TypeListBuilder().apply(block).build()
    }`;

describe('lists', () => {
  it('should return a field fun for an object field', () => {
    expect(toFun(toNode())).toEqual(FIELD_FUN);
  });
});
