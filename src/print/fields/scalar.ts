import { VisitedFieldDefinitionNode } from '../../parse/FieldDefinition';
import { Config } from '../../config';
import { BODY, INDENT } from '../consts';
import { PrintFn } from '../models';

const toTypeName = (config: Config) => (node: VisitedFieldDefinitionNode) =>
  config.scalars[node.type.name] || config.enums[node.type.name];

const shouldWrapInQuotes = (config: Config) => (
  node: VisitedFieldDefinitionNode,
) => config.wrapLiteralsInQuotes[toTypeName(config)(node)];

const toBodyWithQuotes: PrintFn = (node) =>
  `${BODY} += """"${node.name}":"$${node.name}""""`;

const toBodyWithoutQuotes: PrintFn = (node) =>
  `${BODY} += """"${node.name}":$${node.name}"""`;

const toBody = (config: Config) => (node: VisitedFieldDefinitionNode) =>
  shouldWrapInQuotes(config)(node)
    ? toBodyWithQuotes(node)
    : toBodyWithoutQuotes(node);

export const toFun = (config: Config) => (node: VisitedFieldDefinitionNode) =>
  `${INDENT}fun ${node.name}(${node.name}: ${toTypeName(config)(node)}) {
${INDENT + INDENT}${toBody(config)(node)}
${INDENT}}`;
