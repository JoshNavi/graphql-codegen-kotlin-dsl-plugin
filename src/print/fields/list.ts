import { BODY, INDENT } from '../consts';
import { toClosureType, toClosureApply, toListBuilderClassName } from '../util';
import { PrintFn } from '../models';

const toBody: PrintFn = (node) =>
  `${BODY} += """"${node.name}":""" + ${toClosureApply(
    toListBuilderClassName(node.type.name),
  )}`;

export const toFun: PrintFn = (node) =>
  `${INDENT}fun ${node.name}(${toClosureType(
    toListBuilderClassName(node.type.name),
  )}) {
${INDENT + INDENT}${toBody(node)}
${INDENT}}`;
