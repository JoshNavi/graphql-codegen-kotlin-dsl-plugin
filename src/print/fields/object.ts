import { BODY, INDENT } from '../consts';
import {
  toClosureType,
  toClosureApply,
  toBuilderClassName,
  toListBuilderClassName,
  isList,
} from '../util';
import { PrintFn } from '../models';

const toClassName: PrintFn = (node) =>
  isList(node.type)
    ? toListBuilderClassName(node.type.name)
    : toBuilderClassName(node.type.name);

const toBody: PrintFn = (node) =>
  `${BODY} += """"${node.name}":""" + ${toClosureApply(toClassName(node))}`;

export const toFun: PrintFn = (node) =>
  `${INDENT}fun ${node.name}(${toClosureType(toClassName(node))}) {
${INDENT + INDENT}${toBody(node)}
${INDENT}}`;
