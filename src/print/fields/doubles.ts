import { VisitedFieldDefinitionNode } from '../../parse/FieldDefinition';

type Opts = {
  name: string;
  typeName: string;
  typeType: 'list' | 'scalar' | 'object';
};

export const toNode = ({
  name = 'field',
  typeName = 'Type',
  typeType = 'scalar',
}: Partial<Opts> = {}): VisitedFieldDefinitionNode => ({
  name,
  type: {
    name: typeName,
    type: typeType,
  },
});
