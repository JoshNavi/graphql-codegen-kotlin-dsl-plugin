import { toNode } from './doubles';
import { toFun } from './object';

const FIELD_FUN = `    fun field(block: TypeBuilder.() -> Unit) {
        body += """"field":""" + TypeBuilder().apply(block).build()
    }`;

const LIST_FIELD_FUN = `    fun field(block: TypeListBuilder.() -> Unit) {
        body += """"field":""" + TypeListBuilder().apply(block).build()
    }`;

describe('objects', () => {
  it('should return a field fun for an object field', () => {
    expect(toFun(toNode({ typeType: 'object' }))).toEqual(FIELD_FUN);
  });

  it('should return a field fun for an list object field', () => {
    expect(toFun(toNode({ typeType: 'list' }))).toEqual(LIST_FIELD_FUN);
  });
});
