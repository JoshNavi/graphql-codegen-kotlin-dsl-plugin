import { toClosureType, toClosureApply, toBuilderClassName } from './util';

const wrapData = (s: string) => `"""{"data":""" + ${s} + "}"`;

export const toResponse = () =>
  `fun response(${toClosureType(
    toBuilderClassName('Query'),
  )}): String = ${wrapData(toClosureApply(toBuilderClassName('Query')))}`;
