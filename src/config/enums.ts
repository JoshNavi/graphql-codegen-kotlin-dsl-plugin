import { Enums } from './models';

// Maybe one day this is different?
export const toEnumMap = (map: Record<string, string> = {}): Enums => ({
  ...map,
});

export const validate = (scalars: Record<string, string>) =>
  Object.entries(scalars).filter(([_, v]) => typeof v !== 'string');
