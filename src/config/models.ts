export type RawConfig = Record<string, any>;

export type Scalars = {
  ID: string;
  String: string;
  Boolean: string;
  Int: string;
  Float: string;
};

export type Enums = Record<string, string>;

export type Config = {
  scalars: Scalars & Record<string, string>;
  enums: Enums;
  pkg: string;
  verbose: boolean;
  wrapLiteralsInQuotes: Record<string, string>;
};
