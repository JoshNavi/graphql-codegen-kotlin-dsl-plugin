import { Scalars } from './models';

const DEFAULT_SCALARS = {
  ID: 'String',
  String: 'String',
  Boolean: 'Boolean',
  Int: 'Int',
  Float: 'Float',
} as const;

export const toScalarMap = <T>(
  map: Record<string, string> = {},
): Record<T & keyof Scalars, string> => ({
  ...DEFAULT_SCALARS,
  ...map,
});

export const validate = (scalars: Record<string, string>) =>
  Object.entries(scalars).filter(([_, v]) => typeof v !== 'string');
