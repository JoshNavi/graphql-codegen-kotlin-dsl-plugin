import { validate as validateFile } from './file';
import { validate as validateScalars } from './scalars';

describe('config', () => {
  describe('file', () => {
    it('should be a kotlin file', () => {
      expect(validateFile('output.kt')).toBeTruthy();
    });

    it('should not be a kotlin file', () => {
      expect(validateFile('output.js')).toBeFalsy();
    });
  });

  describe('scalars', () => {
    it('should be valid with an emoty object', () => {
      expect(validateScalars({})).toEqual([]);
    });

    it('should be valid with string and int', () => {
      expect(validateScalars({ String: 'NewString', Int: 'NewInt' })).toEqual(
        [],
      );
    });

    it('should be valid with a custom scalar', () => {
      expect(
        validateScalars({
          String: 'NewString',
          NotAScalar: 'String',
          Int: 'NewInt',
        }),
      ).toEqual([]);
    });

    it('should be invalid with a non-string value', () => {
      expect(
        validateScalars({
          String: { a: 'b' },
          Int: 'NewInt',
        } as any),
      ).toEqual([['String', { a: 'b' }]]);
    });
  });
});
