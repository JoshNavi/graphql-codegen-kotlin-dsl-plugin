import { RawConfig } from './models';
import { validate as validateScalars } from './scalars';
import { validate as validateEnums } from './enums';

export const validate = ({ scalars = {}, enums, package: pkg }: RawConfig) => {
  if (!(pkg?.length > 0)) {
    throw Error(`Package name is invalid: ${pkg}`);
  }

  const invalidScalars = validateScalars(scalars);

  if (invalidScalars.length > 0) {
    throw Error(
      `Unrecognized scalars passed in config: ${JSON.stringify(
        invalidScalars,
        null,
        2,
      )}`,
    );
  }

  const invalidEnums = validateEnums(enums);

  if (invalidEnums.length > 0) {
    throw Error(
      `Unrecognized scalars passed in config: ${JSON.stringify(
        invalidEnums,
        null,
        2,
      )}`,
    );
  }
};
