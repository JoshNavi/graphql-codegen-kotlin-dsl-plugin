import { RawConfig, Config } from './models';
import { toScalarMap } from './scalars';
import { toEnumMap } from './enums';

const parse = ({
  scalars = {},
  enums,
  package: pkg,
  verbose = false,
  wrapLiteralsInQuotes = {},
}: RawConfig): Config => ({
  scalars: toScalarMap(scalars),
  enums: toEnumMap(enums),
  verbose,
  pkg,
  wrapLiteralsInQuotes,
});

export default parse;
