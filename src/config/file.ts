export const validate = (fileName: string) => /\.kt$/.test(fileName);
