package name.of.pkg

class StringListBuilder {
    private val body = mutableListOf<String>()

    fun string(value: String) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class BooleanListBuilder {
    private val body = mutableListOf<Boolean>()

    fun boolean(value: Boolean) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class IntListBuilder {
    private val body = mutableListOf<Int>()

    fun int(value: Int) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class FloatListBuilder {
    private val body = mutableListOf<Float>()

    fun float(value: Float) {
        body += value
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class ImageListBuilder {
    private val body = mutableListOf<String>()

    fun image(block: ImageBuilder.() -> Unit) {
        body += ImageBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class AttributeListBuilder {
    private val body = mutableListOf<String>()

    fun attribute(block: AttributeBuilder.() -> Unit) {
        body += AttributeBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class ProductListBuilder {
    private val body = mutableListOf<String>()

    fun product(block: ProductBuilder.() -> Unit) {
        body += ProductBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class CatalogListBuilder {
    private val body = mutableListOf<String>()

    fun catalog(block: CatalogBuilder.() -> Unit) {
        body += CatalogBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "[",
        postfix = "]"
    )
}

class ImageBuilder {
    private val body = mutableListOf<String>()

    fun name(name: String) {
        body += """"name":"$name""""
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}

class AttributeBuilder {
    private val body = mutableListOf<String>()

    fun name(name: String) {
        body += """"name":"$name""""
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}

class ProductBuilder {
    private val body = mutableListOf<String>()

    fun name(name: String) {
        body += """"name":"$name""""
    }

    fun image(block: ImageBuilder.() -> Unit) {
        body += """"image":""" + ImageBuilder().apply(block).build()
    }

    fun attributes(block: AttributeListBuilder.() -> Unit) {
        body += """"attributes":""" + AttributeListBuilder().apply(block).build()
    }

    fun price(price: Int) {
        body += """"price":$price"""
    }

    fun includes(block: StringListBuilder.() -> Unit) {
        body += """"includes":""" + StringListBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}

class CatalogBuilder {
    private val body = mutableListOf<String>()

    fun locale(locale: String) {
        body += """"locale":"$locale""""
    }

    fun accessories(block: ProductListBuilder.() -> Unit) {
        body += """"accessories":""" + ProductListBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}

class QueryBuilder {
    private val body = mutableListOf<String>()

    fun catalog(block: CatalogBuilder.() -> Unit) {
        body += """"catalog":""" + CatalogBuilder().apply(block).build()
    }

    fun build() = body.joinToString(
        separator = ",",
        prefix = "{",
        postfix = "}"
    )
}

fun response(block: QueryBuilder.() -> Unit): String = """{"data":""" + QueryBuilder().apply(block).build() + "}"